BUILDDIR = build
DATADIR = data
MEDIADIR = media
NR = $(shell grep 'Versuchsnummer' metadata.tex | cut -d { -f 3 | cut -d } -f 1)

DATAFILES = $(wildcard $(DATADIR)/**)
MEDIAFILES = $(wildcard $(MEDIADIR)/**)

PYSRC = $(wildcard *.py)
PYOUT = $(patsubst %.py,$(BUILDDIR)/.pymake%,$(PYSRC))

LATEXSRC = $(wildcard *.tex)
PDF = Protokoll_$(NR).pdf
PDFOUT = $(BUILDDIR)/$(PDF)

LATEXMK = max_print_line=1048576 latexmk -e '$$pdf_previewer = "evince %O %S";' -pdf --output-directory=$(BUILDDIR) --synctex=1 --interaction=nonstopmode --halt-on-error --jobname=Protokoll_$(NR) main.tex
PY = python

RM  = rm -rf

GARBAGE = $(BUILDDIR) __pycache__

all: $(PYOUT) $(PDFOUT)

py: $(PYOUT)

pvc:
	@$(LATEXMK) -pvc

pdf: $(PDFOUT)

$(PDFOUT): $(LATEXSRC) $(MEDIAFILES) FORCE | $(BUILDDIR) $(MEDIADIR)
	@echo "building $@"
	@$(LATEXMK)

$(BUILDDIR)/.pymake% : %.py $(DATAFILES) | $(BUILDDIR) $(DATADIR)
	@echo "running $<"
	@$(PY) $<
	@touch $@

$(MEDIADIR) $(DATADIR) $(BUILDDIR):
	@echo "creating $@/"
	@mkdir -p $@
	@touch $@/.empty

clean :
	@echo "cleaning"
	@$(RM) $(GARBAGE)

.PHONY: all clean fast py pdf pvc

FORCE:
