import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.stats import sem # standard error of mean
from uncertainties import ufloat, ufloat_fromstr
import uncertainties.unumpy as unp
from uncertainties.unumpy import (nominal_values as noms, std_devs as stds)
import scipy.constants as const
from latexoutput import latex_table, latex_val

print("INFO Started!\n")

print("\nINFO Finished!")
