# Praktikumsprotokoll #

## Nutzung ##
### build ###
Der *build*-Ordner wird (spätestens) beim ausführen der Makefile erzeugt. In diesem Ordner werden alle erzeugten Dateien, also Plots, Tabellen, PDFs, etc. gespeichert.

** *build* wird bei *make clean* ohne Nachfrage gelöscht, also keine wichtigen Dateien darin speichern!**

### data ###
In diesem Ordner sind alle verwendeten Messdaten gespeichert.

### media ###
In diesem Ordner sind alle im Dokument verwendeten Grafiken gespeichert.

### Abgaben ###
In diesem Ordner werden alle Abgaben gespeichert. Also bitte die erzeugte PDF direkt nach dem Ausdrucken in diesen Ordner kopieren und entsprechend benennen.

*Protokoll_VXX_AbgabeX.pdf*

### metadata.tex ###
In die Datei *metadata.tex* kommt das Versuchsdatum, der Versuchsname und die Versuchsnummer.
Auf diese Variablen kann innerhalb des Dokuments folgendermaßen zugegriffen werden:

	:::latex hl_lines="1"
	Der Versuch \Versuchsnummer \enquote{\Versuchstitel} fand am \Versuchsdatum statt. % Wird durch das definierte Versuchsdatum ersetzt

ergibt beispielsweise:

	Der Versuch V47 "Molwärme von Cu" fand am 16.06.2016 statt.

### main.tex ###
In die Datei *main.tex* kommt der Inhalt des Protokolls.

### header.tex ###
Die Datei *header.tex* beinhaltet alle importierten Pakete und Einstellungen.

Benötigte Pakete können hier einfach ans Ende angehängt werden.

### shortcuts.tex ###
In der *shortcuts.tex* können häufig benötigte Befehle/Symbole/Zeichenketten als neuen Befehl angelegt werden.

Beispiel: Zugriff auf *E_{\gamma}* mit *\Egamma*

	:::latex

	\newcommand{E_{\gamma}}{\Egamma}

### calc.py ###
Die Datei *calc.py* kann als Auswertungsskript der Messdaten verwendet werden.


**Messdaten**

Messdaten bitte in entsprechenden Dateien im Ordner *data* ablegen.
Wenn die Messdaten, per Leerzeichen getrennt, in Spalten (oder Zeilen) in einer Datei abgelegt sind, können sie folgendermaßen in python importiert werden:

	:::python
	import numpy as np

	file = "data/Messdaten.txt"
	data = {}
	data['x'], data['y'] = np.genfromtxt(file)


**LaTeX-Tabellen**

In Python verwendete Daten können ganz einfach in LaTeX-Tabellen exportiert werden. Der folgende Code-Schnipsel exportiert die Daten in den arrays data['x'] und data['y'] in die Datei *tbl_x_y.tex* im *build* Ordner.

	:::python
	from latexoutput import latex_table

	latex_table(name = 'x_y', # Wird gespeichert unter 'build/tbl_x_y.tex'. Tabelle bekommt das LaTeX-Label 'tbl:x_y'
		content = [data['x'],data['y']],
		col_title = r'$x$-Daten, $y$-Daten',
		col_unit = 'mm,mm',
		fmt = '4.1,4.1', # 7.1 erzeugt: mindestens 7 Zeichen langer String (inkl. ',') und 1 Nachkommastelle
		caption = r'Ein x-y-Plot')

Die Tabelle kann dann folgendermaßen in ein LaTeX-Dokument eingebunden werden:

	:::latex
	\input{build/tbl_x_y}

Die Caption muss dabei im Python-Skript definiert werden. Das Label der Tabelle ist *tbl:<NAME>*.


**LaTeX-Werte**

Auch berechnete Werte können einfach in LaTeX exportiert werden. Der Vorteil ist, dass hierbei weniger fehler bei copy+paste entstehen können. Außerdem wird der Wert im LaTeX-Dokument bei jeder Änderung der Berechnung im Skript angepasst.

Für den Export der Variable *data['x']* in die Datei *val_x.tex* im Ordner *build*

	:::python
	from latexoutput import latex_val

	latex_val(name='x',
		val=data['x'],
		unit='mm',
		digits=1)

Für den export aller Werte des Dicts *data*:

	:::python

	for val in data:
		latex_val(name=val,
			val=data[val],
			unit='mm',
			digits=1)

Hierbei geht *val* einmal über alle Keys des Dicts und der Zugriff auf die Werte geschieht via *data[val]*

Die Werte können beispielsweise auf folgende Weise in das LaTeX-Dokument eingebunden werden:

	:::latex

	\begin{equation}
		x = \input{build/val_x}
	\end{equation}


**Fitten**

Ein linearer Fit an die Wertepaare {data['x'],data['y']} kann mit der Funktion *curve_fit* durchgeführt werden. Dabei ist zu beachten, dass die zu fittende Funktion **vor** dem Aufruf von *curve_fit* definiert wird.

	:::python

	import numpy as np
	from scipy.optimize import curve_fit
	from uncertainties import ufloat
	from uncertainties.unumpy import (nominal_values as noms, std_devs as stds)

	def f(x,a,b):
		return a*x + b

	par, cov = curve_fit(f, data['x'], data['y'])
	err = np.sqrt(np.diag(cov))

	fit_a = ufloat(par[0],err[0])
	fit_b = ufloat(par[1],err[1])

In den Arrays *par* und *err* sind die best-fit-Parameter sowie -Fehler gespeichert. Zum späteren Zugriff werden die Werte (mit Fehlern) in die ufloats *fit_a* und *fit_b* geschrieben.


**Plotten**

Zum Plotten kann matplotlib verwendet werden.

	:::python

	import matplotlib.pyplot as plt

	x_range = np.max(data['x'])-np.min(data['y'])
	x_plot = np.linspace(np.min(data['y'])-0.05*x_range,np.max(data['x'])-0.05*x_range,100)

	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.plot(data['x'],data['y'],'rx',label='Messdaten')
	ax.plot(x_plot,f(x_plot,noms(fit_a),noms(fit_b)),'b-',label='Fit')
	ax.set_xlabel("x")
	ax.set_ylabel("y")
	ax.set_xlim(np.min(x_plot),np.max(x_plot))
	ax.legend(loc = 'best')
	plt.savefig("build/plt_%s.pdf" % ('x-y-Plot'))
	plt.close()

Der erzeugte Plot wird als *plt_x-y-Plot.pdf* im *build*-Ordner gespeichert.

Das einbinden in LaTeX erfolgt folgendermaßen:

	:::latex

	\begin{figure}
	  \centering
		\includegraphics[width=.8\textwidth]{build/plt_x-y-plot.pdf}
	  \caption{Messdaten mit linearem Fit}
	  \label{plt:x-y-Plot}
	\end{figure}

Wichtig: Das Label darf erst **nach** der Caption definiert werden!

## Git ##

Am besten **vor** dem Beginn der Bearbeitung einmal den aktuellen Stand vom Server holen:

	:::bash

	git pull

Nach der Bearbeitung: Zum Index hinzufügen, Committen, Pushen

	:::bash

	git add -A # -A fügt alle Dateien hinzu, ansonsten Dateinamen angeben
	git commit -m "COMMIT_MESSAGE"
	git pull # Zur Sicherheit vor dem pushen nochmal pullen
	git push

## Make ##

Das Praktikumsprotokoll ist mit einer Makefile konfiguriert.

Folgende Befehle stehen zur Verfügung

	:::bash

	make # Führt die Python Datei aus und erstellt danach die PDF Datei
	make all # wie make

	make pdf # Erstellt nur die PDF Datei und ignoriert änderungen am Python Skript
	make pvc # Erstellt die PDF Datei, öffnet eine Vorschau und wartet. Erstellt bei Änderungen die PDF sofort neu.

	make py # Führt nur das Python Skript aus

	make clean # Löscht den build und den __pycache__ Ordner
