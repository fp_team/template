import os
from itertools import zip_longest

def latex_table(name, content, row_title='', col_title=[], col_unit=[], caption='', label='', fmt=[], unpack=False):
	file_name = os.path.join("build", 'tbl_%s.tex' % name)

	content = list(zip_longest(*content))						# fill the 2dim list to a square (side effect: swaps axes)
	if unpack:
		content = list(zip_longest(*content))					# swap axes when unpacking is true

	if row_title: row_title = row_title.split(sep=',')			# get string params into lists
	if col_title: col_title = col_title.split(sep=',')
	if col_unit: col_unit = col_unit.split(sep=',')
	if fmt: fmt = fmt.split(sep=',')

	if row_title and len(content[0]) == len(col_title):			# if each content col gets a title, insert empty element for row_title
		col_title.insert(0, '')
		col_unit.insert(0, '')
		fmt.insert(0, '')

	xdim = len(content[0])										# get the width of content (is already squared)
	if row_title: xdim +=1										# and add width of row_title column if exists

	if(label==''):
		label = 'tbl:%s' % name

	str_result = '\\begin{table}[ht]\n\\centering\n\\caption{'	# latex syntax for a table with caption and label
	str_result += caption
	str_result += '}\n\\label{'
	str_result += label
	str_result += '}\n\sisetup{table-auto-round}'				# round to specified format value
	str_result += '\n\\begin{tabular}{'
	for i in range(xdim):
		str_result += ' S'										# use S column
		if i < len(fmt):
			if fmt[i]:
				str_result += '[table-format = '+fmt[i]+', '
				str_result += 'table-figures-uncertainty=1]'	# set format
	str_result += '}\n\\toprule\n'

	str_top = ''												# write top row with title and unit
	for i in range(xdim):
		str_top += '{'
		if i < len(col_title): str_top += col_title[i]
		if i < len(col_unit):
			if col_unit[i]: str_top += ' [\\si{' + col_unit[i] + '}]'	# siunitx package required
		str_top += '}'
		str_top += ' & '
	str_top = str_top[:-3] + r'\\'								# delete last &
	str_result += str_top										# add top row to the latex code
	str_result += '\n\\midrule\n'

	for index, x in enumerate(content):							# get index and data for each row
		if row_title:											# write row_title
			if index < len(row_title):
				str_result += '{'+row_title[index]+'}'
			str_result += ' & '
		for val in x:											# write values
			if val or val == 0:
				if type(val).__name__ in ['Variable','AffineScalarFunc']:
					str_val = '{:.1ufS}'.format(val)			# 1 digit for the uncertainty, shorthand
				elif type(val).__name__ in ['float', 'int', 'float64']:
					str_val = str(val)							# float/int output
				else:
					str_val = '{'+str(val)+'}'					# brackets for no interpretation by siunitx; str etc.
				str_result += str_val
			str_result += ' & '
		str_result = str_result[:-3]							# delete last &
		str_result += '\\\\\n'

	str_result += '\\bottomrule\n\\end{tabular}\n\\end{table}'

	if not os.path.exists(os.path.dirname(file_name)):			# create folder if not existing
		os.makedirs(os.path.dirname(file_name))
	file = open(file_name, 'w', encoding='utf-8')				# write the generated code to the specified file
	file.write(str_result)
	file.close()


def latex_val(name, val, unit='', digits=None):
	file_name = os.path.join("build", 'val_%s.tex' % name)

	if type(val).__name__ == 'Variable' or type(val).__name__ == 'AffineScalarFunc':
		str_val = '{:.1ufS}'.format(val)						# 1 digit for the uncertainty, shorthand
	else:
		if digits != None:
			str_val = ('{:.'+str(digits)+'f}').format(val)		# float rounding
		else:
			str_val = str(val)									# no precision specified

	str_result = '\\SI{'+str_val+'}{'+unit+'}'

	if not os.path.exists(os.path.dirname(file_name)):			# create folder if not existing
		os.makedirs(os.path.dirname(file_name))
	file = open(file_name, 'w', encoding='utf-8')
	file.write(str_result)
	file.close()
